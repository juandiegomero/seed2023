/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Estudiante;
import Util.seed.ArchivoLeerURL;
import Util.seed.ListaCD;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DOCENTE
 */
public class SIA {

    private ListaCD<Estudiante> fisica;
    private ListaCD<Estudiante> estructuras;
    private ListaCD<Estudiante> poo;

    public SIA(String urlFisica, String urlestructura, String urlpoo ) {
        this.fisica = crear(urlFisica);
         this.estructuras = crear(urlestructura);
          this.poo = crear(urlpoo);
    }
    

    private ListaCD<Estudiante> crear(String url) {
        ListaCD<Estudiante> l = new ListaCD();
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //16980,NOMBRE 1,0,2,4,4
            String datos[] = fila.split(",");
            long codigo = Long.parseLong(datos[0]);
            String nombre = datos[1];
            float p1 = Float.parseFloat(datos[2]);
            float p2 = Float.parseFloat(datos[3]);
            float p3 = Float.parseFloat(datos[4]);
            float exm = Float.parseFloat(datos[5]);
            Estudiante nuevo = new Estudiante(codigo, nombre, p1, p2, p3, exm);
            l.insertarFinal(nuevo);
        }
        return l;
    }

    public String getListadoFisica() {
        String msg = "";
        for (Estudiante dato : this.fisica) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }
    public String getListadoestructura() {
        String msg = "";
        for (Estudiante dato : this.estructuras) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }
    public String getListadopoo() {
        String msg = "";
        for (Estudiante dato : this.poo) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<String> getListadoFinal() {
        ListaCD<String> resultado = new ListaCD();
        for (Estudiante x : this.fisica) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m+"\n");
        }
        return resultado;
    }
    
    public void getInformePDF(ListaCD<String> listaestu)
    {
        try {
            new ImpresoraInformeSIA().imprimirListadoPromedio(listaestu);
        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(SIA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ListaCD<String> RrecorrerLista(ListaCD<Estudiante> ListadeEstudiantes) {
        ListaCD<String> listaresultado = new ListaCD();

        for (Estudiante x : ListadeEstudiantes) {

            listaresultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + EncontrarMateria(x) + "-");
        }
        return listaresultado;

    }

    public ListaCD<Estudiante> getlistatotal() {

        ListaCD<Estudiante> estudiantes = new ListaCD();

        agregarEstudiantes(estudiantes, this.fisica);
        agregarEstudiantes(estudiantes, this.estructuras);
        agregarEstudiantes(estudiantes, this.poo);

        return estudiantes;

    }

    private void agregarEstudiantes(ListaCD<Estudiante> listae, ListaCD<Estudiante> materiia) {
        
        for (int i=0;i<materiia.getSize(); i++ ) {
            Estudiante u = materiia.get(i);
            if (!listae.containTo(u)) {
                listae.insertarFinal(u);
            }
        }

    }

    // metodo para encontrar el estudiante en las listas 
    public String EncontrarMateria(Estudiante E) {
        String respuesta = "";
        if (this.fisica.containTo(E)) {
            respuesta += "fisica," ;

        }
        if (this.estructuras.containTo(E)) {
            respuesta += " estruct," ;

        }
        if (this.poo.containTo(E)) {
            respuesta += " poo" ;
        }

        return respuesta;
    }

}
