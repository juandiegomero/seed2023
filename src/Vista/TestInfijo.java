/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Cola;
import Util.seed.Pila;

/**
 *
 * @author JUAN D MELENDEZ
 */
public class TestInfijo {
    
    
    public static void main(String[] args) {
        String cadena="UFPS&SPF"; //--> true
        String cadena2="hola&olot";
        /**
         * LO QUE ESTÁ ANTES DEL & SE COLOCA EN UNA COLA
         * LO QUE ESTÁ DESPUÉS EN UNA PILA
         * Y SE COMPARA LA PILA Y LA COLA
         */
        
        
        String[] parts = cadena.split("&");
        String[] parts2 = cadena2.split("&");

        Pila<Character> pila = new Pila();
        Cola<Character> cola = new Cola();


        for (char c : parts2[0].toCharArray()) {
            pila.push(c);
        }


        for (char c : parts2[1].toCharArray()) {
            cola.enColar(c);
        }


        boolean isInverso = true;
        while (!pila.isEmpty() && !cola.isEmpty()) {
            if (pila.pop() != cola.deColar()) {
                isInverso = false;
                break;
            }
        }


        System.out.println(isInverso);
    }

        
        
        
        
        
        
    }
    


